springcloud配置中心server启动报错： Authentication is required but no CredentialsProvider has been registered
猜测 是在github上创建的仓库是私有的 ，所以 需要安全连接到github仓库，在application.yml中配置上自己github 的用户名和密码 就可以了：


spring:
  application:
    #设置应用名字
    name: cloud-config-server

  cloud:
    config:
      server:
        git:
          #git仓库地址  在git创库复制 https的地址
          uri: https://gitee.com/mo-haihua/cloud-config2233.git
          #占位符url 可以不写
          search-paths:
            - cloud-config2233
          #http://localhost:3344/config-test.yml
          #如果访问不到 报错Authentication is required but no CredentialsProvider has been registered
          #把git仓库改为开源  ，私有是访问不了   或者设置git用户名与密码
          #force-pull: true
#          username:
#          password:

      # GIT 仓库中的哪个分支
      lable: master



还有个地方，就是 spring.cloud.config.server.git.uri 地址有的用 ssh的方式 会报错 改成 https 的方式就可以了
