[root@localhost ~]# docker pull nacos/nacos-server
Using default tag: latest
Trying to pull repository docker.io/nacos/nacos-server ... 
manifest for docker.io/nacos/nacos-server:latest not found



找不到镜像

[root@localhost ~]# vim /etc/docker/daemon.json 
添加阿里镜像：
{
  "registry-mirrors": ["https://wt18sp0p.mirror.aliyuncs.com"]
}
或：
{
 "exec-opts": ["native.cgroupdriver=systemd"],
 "registry-mirrors": ["https://32xw0apq.mirror.aliyuncs.com"]
}


[root@localhost ~]# systemctl daemon-reload
[root@localhost ~]# systemctl restart docker
[root@localhost ~]# docker pull nacos/nacos-server:latest


启动nacos镜像
单机版部署
docker run --name nacos -d  -p 8848:8848 -e MODE=standalone -e NACOS_SERVER_IP=192.168.66.105 nacos/nacos-server
参数：

MODE：单节点模式
NACOS_SERVER_IP：服务ip地址
测试
请求http://192.168.66.105:8848/nacos