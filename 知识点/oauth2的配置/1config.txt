创建config文件夹

创建AuthorizationServerConfig类


package com.itbaizhan.clouddemooauthserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import javax.sql.DataSource;

/**
 * 认证授权配置类
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private DataSource dataSource; //在application已经配置

   // AuthenticationManager是一个接口，要Bean
    //注入到容器中，才能使用,在SecurityConfigurer类中注入
    @Autowired
    private AuthenticationManager authenticationManager;
    /**
     * 认证授权服务器以接口调用的方式对外提供服务，因此涉及到接口的访问权限问题，接口的安全配置写在这个方法里
     * @param security
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        super.configure(security);
        security.allowFormAuthenticationForClients() //开启客户端表单认证
                .tokenKeyAccess("permitAll()") //开启端口/oauth/token的访问权限（允许所有）
                .checkTokenAccess("permitAll()"); //开启端口/oauth/check_token的访问权限（允许所有）
    }

    /**
     * 加载客户信息，可以写死在方法中，也可以通过数据库查询客户信息,客户信息可以是从第三方网站获取
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        super.configure(clients);
//        clients.inMemory() //客户信息写在内存中
//                .withClient("itbaizhan_cloud_demo")//添加一个client_id,并指定一个名称
//                .secret("abcde")//客户端密码，
//                .resourceIds("cloud-demo-product","product-demo-order")//资源id,微服名
//                .authorizedGrantTypes("password","refresh_token")//令牌颁发的方式，采用密码式
//                .scopes("all");//权限范围



        //改成从数据库中加载客户端，
        // withClientDetails()的括号里面填ClientDetailsService的接口，而接口里面有两个实现类
        //其中一个是JdbcClientDetailsService(),操控数据库，使用这类要在数据库创建oauth_client_details表
        //进入这类可以看到表的字段
        //那么withClientDetails()括号里可以写JdbcClientDetailsService()的对象
        clients.withClientDetails(jdbcClientDetailsService());
    }


    @Bean
    public JdbcClientDetailsService jdbcClientDetailsService(){
        //因为JdbcClientDetailsService 是实现类，所以可以new
        JdbcClientDetailsService detailsService = new JdbcClientDetailsService(dataSource);
        return detailsService;
    }

    /**
     * 配置令牌的属性，例如：令牌的有效期，令牌的存储方式等
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        super.configure(endpoints);
        endpoints.tokenStore(tokenStore()) //指定token的存储方法
                .tokenServices(authorizationServerTokenServices()) //token生成的细节描述，比如：token的有效期
                .authenticationManager(authenticationManager) //通过注入的方式传入授权管理对象
                .allowedTokenEndpointRequestMethods(HttpMethod.GET,HttpMethod.POST);//请求的方法类型
    }

    //用于创造令牌的存储对象,
    public TokenStore tokenStore(){
        //现在先用内存的方式进行测试,后期会更改为JWT方式
        //return new InMemoryTokenStore();
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    //密钥
    private static String sign_key = "itbaizhan";

    /**
     *
     * @return
     */
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(sign_key); //签名密钥
        jwtAccessTokenConverter.setVerifier(new MacSigner(sign_key)); //验证时使用的密钥，和签名密钥一样
        return jwtAccessTokenConverter;
    }

    /**
     * 此方法获取令牌服务对象 （它描述了token的有效期等信息）
     * @return
     */
    public AuthorizationServerTokenServices authorizationServerTokenServices(){
        //使用默认的实现类
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        //是否开启刷新令牌
        defaultTokenServices.setSupportRefreshToken(true);
        //设置令牌的存储方式
        defaultTokenServices.setTokenStore(tokenStore());
        //针对jwt令牌的增强
        defaultTokenServices.setTokenEnhancer(jwtAccessTokenConverter());
        //设置令牌的有效期
        defaultTokenServices.setAccessTokenValiditySeconds(20); //access_token
        //设置刷新令牌的有效期  这里是一天
        defaultTokenServices.setRefreshTokenValiditySeconds(84400);
        return defaultTokenServices;
    }
}
/*CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(255) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;*/