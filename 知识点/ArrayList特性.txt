因为在ArrayList中，元素是按照添加顺序依次存放的，所以他每次进去都是第一个

例如,当秒杀商品下单时，从购物车中获得商品，会是list集合的第一个，所以索引是0

//购物车里有很多商品，怎么确定这商品是下单的商品
        // 因为在ArrayList中，元素是按照添加顺序依次存放的，所以他每次进去都是第一个
        //当商品下单，它会重新成为在list中的第一个




为了让用户购买速度更快，秒杀商品时不会将商品添加到购物车，而是直接生成订单。并且由于访问量较大，为了避免数据库压力过大，我们会先将订单数据保存在redis当中，等用户支付完成后，再将redis中的订单数据保存到数据库中

 @Autowired
    private RedisTemplate redisTemplate;

//生成秒杀订单，因为
@Override
    public Orders createOrder(Orders orders) {
        //生成订单对象  因为订单是在redis中生成，所以不是在数据库中，不会自动用雪花算法生成id
        //IdWorker.getIdStr() 这是mybatisPlus 包的，可以手动生成id
        orders.setId(IdWorker.getIdStr());
        orders.setStatus(1); //未付款
        orders.setCreateTime(new Date()); //订单创建时间
        orders.setExpire(new Date(new Date().getTime()+1000*60*5)); //订单过期时间,5分钟

        //计算商品价格 ，
        //购物车里有很多商品，怎么确定这商品是下单的商品
        // 因为在ArrayList中，元素是按照添加顺序依次存放的，所以他每次进去都是第一个
        //当商品下单，它会重新成为在list中的第一个
        CartGoods cartGoods = orders.getCartGoods().get(0);
        Integer num = cartGoods.getNum();
        BigDecimal price = cartGoods.getPrice();
        BigDecimal sum = price.multiply(BigDecimal.valueOf(num));  //BigDecimal.valueOf  num转为BigDecimal类型
        orders.setPayment(sum);

        //2减小秒杀商品库存
        //查询秒杀商品
        SeckillGoods seckillGoodsByRedis = findSeckillGoodsByRedis(cartGoods.getGoodId());
        //查询库存，库存不足抛出异常
        Integer stockCount = seckillGoodsByRedis.getStockCount();
        if(stockCount <=0){
            throw  new BusException(CodeEnum.NO_STOCK_ERROR);
        }
        //减小库存
        seckillGoodsByRedis.setStockCount(seckillGoodsByRedis.getStockCount() - cartGoods.getNum());
        redisTemplate.boundHashOps("seckillGoods").put(seckillGoodsByRedis.getGoodsId(),seckillGoodsByRedis);
        //保存订单数据,键值对保存
        /*不理解redis的key为什么要序列化？
        任何存储都需要序列化。只不过常规你在用DB一类存储的时候，
        这个事情DB帮你在内部搞定了（直接把SQL带有类型的数据转换成内部序列化的格式，
        存储；读取时再解析出来）。而Redis并不会帮你做这个事情*/
        /*为什么要设置这个序列化
        序列化最终的目的是为了对象可以跨平台存储，和进行网络传输。
        而我们进行跨平台存储和网络传输的方式就是IO，
        而我们的IO支持的数据格式就是字节数组，本质上存储和网络传输 都需要经过 把一个对象状态保存成一种跨平台识别的字节格式，
        然后其他的平台才可以通过字节信息解析还原对象信息。*/
        /*为什么要用StringRedisSerializer()
        使用默认的那种方式，序列化后的结果非常庞大，是JSON格式的5倍左右，
        这样就会消耗redis服务器的大量内存。*/
        redisTemplate.setKeySerializer(new StringRedisSerializer()); //序列化
        redisTemplate.opsForValue().set(orders.getId(),orders);

        return orders;
    }