/**
     * 保存用户和角色之间的关系，角色有很多：例如：医生、护士、发药等
     * @param userId
     * @param roleIds
     * @return
     */
     @PostMapping("saveRoleuser/{userId}/{roleIds}")
    public AjaxResult saveRoleUser(@PathVariable Long userId,@PathVariable Long[] roleIds){
         //判断前端传来的roleIds有没有选中,如果没有选
         //传入来的第一个是-1，说明没有选择角色
         if(roleIds.length ==1 && roleIds[0].equals(-1l)){
             roleIds = new Long[]{};
         }
         roleService.saveRoleUser(userId, roleIds);
         return AjaxResult.success();
     }




public interface RoleService extends IService<Role> {

    int saveRoleUser(Long userId,Long[] roleIds);}



@Override
    public int saveRoleUser(Long userId, Long[] roleIds) {
        //先删除了角色里的用户id,再重新保存，
        //编写sql语句，根据用户与角色的关联表删除
        roleMapper.deleteRoleUserByUserIds(Arrays.asList(userId));
        for(Long rolId: roleIds){
            this.roleMapper.saveRoleUser(userId,rolId);
        }
        return 0;
    }