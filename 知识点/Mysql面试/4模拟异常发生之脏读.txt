前置知识
# 查看 MySQL 版本
select version();
 
# 开启事务
start transaction;
 
# 提交事务
commit;
 
# 回滚事务
rollback;
查看连接的客户端详情
每个 MySQL 命令行窗口就是一个 MySQL 客户端，每个客户端都可以单独设置（不同的）事务隔离级别，这也是演示 MySQL 并发事务的基础。

show processlist;


新建数据库和测试数据
-- 创建数据库
drop database if exists testdb;
create database testdb;
use testdb;
-- 创建表
create table userinfo(
  id int primary key auto_increment,
  name varchar(250) not null,
  balance decimal(10,2) not null default 0
);
-- 插入测试数据
insert into userinfo(id,name,balance) values(1,'Java',100),(2,'MySQL',200);

查询事务的隔离级别
select @@global.transaction_isolation,@@transaction_isolation;



设置客户端的事务隔离级别
通过以下 SQL 可以设置当前客户端的事务隔离级别：

set session transaction isolation level 事务隔离级别;
例：
set session transaction isolation level READ UNCOMMITTED;
事务隔离级别的值有 4 个：

READ UNCOMMITTED：读未提交
READ COMMITTED：读已提交
REPEATABLE READ：可重复读
SERIALIZABLE：串行化

脏读
一个事务读到另外一个事务还没有提交的数据，称之为脏读。脏读演示的执行流程如下：

执行步骤			
第 1 步	  客户端2（窗口2）	
set session transaction isolation level read uncommitted; 
start transaction; 
select * from userinfo;	

说明
设置事务隔离级别为读未提交； 开启事务； 查询用户列表，其中 Java 用户的余额为 100 元。

第 2 步	客户端1（窗口1）
start transaction; update userinfo set balance=balance+50 where name='Java';	
说明 	
开启事务； 给 Java 用户的账户加 50 元；

第 3 步	 客户端2（窗口2）	
select * from userinfo;	
说明 	
查询用户列表，其中 Java 用户的余额变成了 150 元。





脏读演示步骤1
设置窗口 2 的事务隔离级别为读未提交，设置命令如下：

set session transaction isolation level read uncommitted;



意：

事务隔离级别读未提交存在脏读的问题。

脏读演示步骤2
窗口2开启事务，查询用户表如下图所示：

start transaction;
select * from userinfo;

注意:

从上述结果可以看出，在窗口 2 中读取到了窗口 1 中事务未提交的数据，这就是脏读。


脏读演示步骤3
在窗口 1 中开启一个事务，并给 Java 账户加 50 元，但 不提交事务，执行的 SQL 如下：

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> update userinfo set balance=balance+50 where name="java";
Query OK, 1 row affected (0.00 sec)
Rows matched: 1 Changed: 1 Warnings: 0


脏读演示步骤4
在窗口 2 中再次查询用户列表，执行结果如下：
select * from userinfo

注意：
(开启事务后start transaction;只是执行了sql语号，还没有提交事务commit;第二个事务就会读取到没有被提交的脏数据)
从上述结果可以看出，在窗口 2 中读取到了窗口 1 中事务未提交的数据，这就是脏读。