//修改所有用户购物车商品方法
    /*当管理员修改了商品的价格等数据后，需要将数据同步到用户购物车中，
    所以我们要编写给所有用户的购物车中修改某件商品的方法*/

    @Override
    public void refreshCartGoods(CartGoods cartGoods) {
        //1 获取所有用户购物车商品
        BoundHashOperations cartList = redisTemplate.boundHashOps("cartList");
        //转为map集合
        Map<Long,List<CartGoods>> allCartGoods = cartList.entries();
        //转为set
        Set<Map.Entry<Long, List<CartGoods>>> entries = allCartGoods.entrySet();
        //2 遍历所有用户的购物车
        for(Map.Entry<Long, List<CartGoods>> entry:entries){
            List<CartGoods> goodsList = entry.getValue(); //获得用户在购物车的商品
           // 遍历一个用户购物车的所有商品
            for(CartGoods goods : goodsList){
                //如果该商品是被更新的商品，修改商品数量
                if(cartGoods.getGoodId().equals(goods.getGoodId())){
                    goods.setGoodsName(cartGoods.getGoodsName());
                    goods.setHeaderPic(cartGoods.getHeaderPic());
                    goods.setPrice(cartGoods.getPrice());
                }
            }

            // 将改变后所有用户购物车重新放入redis
            //删除原来的
            redisTemplate.delete("cartList");
            redisTemplate.boundHashOps("cartList").putAll(allCartGoods);

        }

    }











/*当管理员下架某件商品后，所有用户的购物车中该商品也应该被删除，
    所以我们要编写给所有用户的购物车中删除某件商品的方法。*/

    @Override
    public void deleteCartGoods(CartGoods cartGoods) {
        //1 获取所有用户购物车商品
        BoundHashOperations cartList = redisTemplate.boundHashOps("cartList");
        //转为map集合
        Map<Long,List<CartGoods>> allCartGoods = cartList.entries();
        //转为set
        Set<Map.Entry<Long, List<CartGoods>>> entries1 = allCartGoods.entrySet();
        //2 遍历所有用户的购物车
        for(Map.Entry<Long, List<CartGoods>> entry:entries1){
            //获得用户在购物车的商品
            List<CartGoods> goodsList = entry.getValue();
            //遍历一个用户购物车的所有商品
            for(CartGoods goods : goodsList){
                if(cartGoods.getGoodId().equals(goods.getGoodId())){
                    goodsList.remove(goods);  //从list集合的商品，删除符合要求的商品
                    break;  //结束当前循环，继续下一个循环，如果是return则是返回，结束所有循环
                }
            }
        }

        // 将改变后的map重新放入redis
        //删除原来的
        redisTemplate.delete("cartList");
        redisTemplate.boundHashOps("cartList").putAll(allCartGoods);
    }